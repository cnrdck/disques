# Disqu.es

Un méta-moteur de recherche pour faciliter l'acquisition régulière de disques et 
l’optimisation des frais de port.

WIP mais disponible sur [disqu.es](https://disqu.es)

## Développement

Le développement se passe sur [GitLab](https://gitlab.com/cnrdck/disques).

![build status](https://gitlab.com/cnrdck/disques/badges/master/build.svg)
![coverage report](https://gitlab.com/cnrdck/disques/badges/master/coverage.svg)