# Utilisez `make help` pour consulter l'aide.

dev: ## Lancer le serveur de dev
	./app/manage.py runserver

tests:  ## Tests unitaires, arguments pytest avec args="--lf"
	python -m pytest $(args)

qcluster:  ## Démarrage des runners django-q
	./app/manage.py qcluster

requirements: ## Maj des dependances puis syncho de l'environnement
	python -m pip install pip-tools
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--output-file=requirements.txt pyproject.toml
	python -m piptools compile --upgrade --resolver backtracking --verbose --extra=dev \
		--output-file=requirements.dev.txt pyproject.toml
	python -m piptools sync requirements.dev.txt

shell: ## shell django
	./app/manage.py shell

lint:  ## Linting
	python -m ruff check app --fix 
	python -m mypy app

manage: ## manage.py + arguments passés avec args, ex. args="makemigrations xxx"
	./app/manage.py $(args)

dump_shops: ## Dumpdata des boutiques
	./app/manage.py dumpdata --indent 4 search.Shop > app/search/fixtures/shops.json

load_shops: ## Loadata des boutiques
	./app/manage.py loaddata shops.json

info: header
define HEADER
   ..          .       .x+=:.                                            .x+=:.   
 dF           @88>    z`    ^%                                          z`    ^%  
'88bu.        %8P        .   <k                x.    .                     .   <k 
'*88888bu      .       .@8Ned8"     .u@u     .@88k  z88u        .u       .@8Ned8" 
  ^"*8888N   .@88u   .@^%8888"   .zWF8888bx ~"8888 ^8888     ud8888.   .@^%8888"  
 beWE "888L ''888E` x88:  `)8b. .888  9888    8888  888R   :888'8888. x88:  `)8b. 
 888E  888E   888E  8888N=*8888 I888  9888    8888  888R   d888 '88%" 8888N=*8888 
 888E  888E   888E   %8"    R88 I888  9888    8888  888R   8888.+"     %8"    R88 
 888E  888F   888E    @8Wou 9%  I888  9888    8888 ,888B . 8888L        @8Wou 9%  
.888N..888    888&  .888888P`   `888Nx?888   "8888Y 8888"  '8888c. .+ .888888P`   
 `"888*""     R888" `   ^"F      "88" '888    `Y"   'YP     "88888%   `   ^"F     
    ""         ""                      88E                    "YP'                
                                       98>                                        
                                       '8                                         
                                        `
endef
export HEADER

.PHONY: all

help:
	@echo "$$HEADER"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
