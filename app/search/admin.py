"""Admin search."""

from django.contrib import admin
from search import models


@admin.register(models.Search)
class SearchAdmin(admin.ModelAdmin):
    """Gestion des recherches."""

    date_hierarchy = "digged_at"
    ordering = ["-created_at"]
    list_display = ("query", "medium", "created_at", "viewed_at", "digged_at")
    search_fields = ("query",)
    list_filter = ("medium",)
    prepopulated_fields = {"slug": ("query",)}


@admin.register(models.Shop)
class ShopAdmin(admin.ModelAdmin):
    """Gestion des disquaires."""

    list_display = (
        "name",
        "digger_name",
        "created_at",
        "region",
        "is_independent",
        "is_bm",
        "is_marketplace",
        "is_direct",
        "is_active",
    )
    search_fields = ("name", "url", "description")
    list_filter = (
        "region",
        "digger_name",
        "is_independent",
        "is_bm",
        "is_marketplace",
        "is_direct",
        "is_active",
    )
    prepopulated_fields = {"slug": ("name",)}


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    """Gestion des produits."""

    date_hierarchy = "created_at"
    list_display = (
        "title",
        "medium",
        "condition",
        "shop",
        "price_in_eur",
        "availability",
    )
    search_fields = ("title", "description")
    list_filter = (
        "shop",
        "medium",
        "availability",
        "condition",
    )


@admin.register(models.Dig)
class DigAdmin(admin.ModelAdmin):
    """Gestion des digs."""

    date_hierarchy = "started_at"
    list_display = (
        "search",
        "shop",
        "status",
        "started_at",
        "ended_at",
    )
    ordering = ["-started_at"]
    search_fields = ("search__query",)
    list_filter = (
        "shop",
        "status",
    )


@admin.register(models.Result)
class ResultAdmin(admin.ModelAdmin):
    """Gestion des résultats de dig."""

    ordering = ["-created_at"]
    list_display = (
        "search",
        "dig",
        "product",
        "created_at",
    )
