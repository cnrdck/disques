"""Factories."""

import factory
from django.conf import settings
from factory import fuzzy

from . import models


class SearchFactory(factory.Factory):
    """Recherche."""

    class Meta:
        model = models.Search

    query = factory.Faker("bs")
    media = fuzzy.FuzzyChoice(settings.MEDIA_CHOICES, getter=lambda m: m[0])


class ShopFactory(factory.Factory):
    """Disquaire."""

    class Meta:
        model = models.Shop

    name = factory.Faker("company")
    url = factory.Faker("uri")
    logo_url = factory.Faker("uri")
    region = factory.fuzzy.FuzzyChoice(settings.REGION_CHOICES, getter=lambda r: r[0])
    is_independent = factory.Faker("boolean")
    is_bm = factory.Faker("boolean")
    is_marketplace = factory.Faker("boolean")
    is_direct = factory.Faker("boolean")


class ProductFactory(factory.Factory):
    """Générateur de produits."""

    class Meta:
        model = models.Product

    shop = factory.SubFactory(ShopFactory)
    title = factory.Faker("catch_phrase")
    medium = fuzzy.FuzzyChoice(settings.MEDIA_CHOICES, getter=lambda m: m[0])
    # TODO: maybe image_url
    # TODO: maybe description
    url = factory.Faker("uri")  # FIXME: utiliser url relative à celle du shop
    price = factory.Faker(
        "pyfloat", right_digits=2, positive=True, min_value=1, max_value=200
    )
    availability = fuzzy.FuzzyChoice(
        settings.AVAILABILITY_CHOICES, getter=lambda a: a[0]
    )
    condition = fuzzy.FuzzyChoice(settings.CONDITION_CHOICES, getter=lambda c: c[0])
