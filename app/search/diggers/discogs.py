"""Marketplace discogs."""

from collections.abc import Iterator

from core.helpers import extract_amount, extract_currency
from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product


class Digger(CrateDigger):
    """Discogs."""

    base_url = "https://www.discogs.com"
    search_url = base_url + "/sell/list"

    def __init__(self, query, medium=None, shop=None):
        """Si shop est précisé on change la search_url.

        Il faut préciser dans le shop l'url du profile seller, ex:
        * shop.url https://www.discogs.com/fr/seller/Blind_Spot/profile
          (pas /user/Blind_Spot/)
        """
        super().__init__(shop=shop, query=query, medium=medium)
        if shop:
            self.search_url = shop.url

    @property
    def media_matchings(self) -> dict[str, str]:
        """Correspondance media."""
        return {
            "vinyl": settings.MEDIA_VINYL,
            "cd": settings.MEDIA_CD,
            "cassette": settings.MEDIA_K7,
        }

    def search(self) -> Iterator[str]:
        """Recherche sur le moteur de la market place.

        Les liens sont dans une table.
        """
        payload = {}
        payload["q"] = self.query
        if self.medium == settings.MEDIA_VINYL:
            payload["format"] = "Vinyl"
        elif self.medium == settings.MEDIA_CD:
            payload["format"] = "CD"
        elif self.medium == settings.MEDIA_K7:
            payload["format"] = "Cassette"
        soup = self.fetch(self.search_url, payload)
        table = soup.find("table", class_="mpitems")
        for item in table.find_all("tr", class_="shortcut_navigable"):
            link = item.find("a", class_="item_description_title")
            logger.debug("Trouvé le lien %s", link["href"])
            yield self.base_url + link["href"]

    def extract(self, url: str) -> Iterator[Product]:
        """Un seul disque par page."""
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return
        product = Product()
        product.shop = self.shop
        product.url = url  # un seul produit par page
        product.availability = settings.AVAILABILITY_IN  # pas de précommande/backorder
        product.title = (
            soup.find("h1", id="profile_title")
            .get_text(strip=True)
            .replace("\u200e", "")
        )
        product.image_url = soup.find("span", class_="thumbnail_center").img["src"]

        price_and_currency = (
            soup.find("span", class_="price").get_text(strip=True).replace("\u200e", "")
        )
        currency = extract_currency(price_and_currency)
        amount = extract_amount(price_and_currency)
        product.price = Money(amount, currency)

        # l'icône (i) contient l'état & le format du média
        medium_condition = soup.find("i", class_="media-condition-tooltip")
        if medium_condition:
            condition = medium_condition["data-condition"]
            # tout est d'occasion sauf Mint
            if condition.lower() == "mint (m)":
                product.condition = settings.CONDITION_NEW
            else:
                product.condition = settings.CONDITION_USED
            found_medium = medium_condition["data-format"]
        else:
            # tooltip manquant ?
            product.condition = settings.CONDITION_UNK
            found_medium = settings.MEDIA_UNK

        if not self.media_match(found_medium):
            logger.debug("Mauvais format, on ignore")
            return

        product.medium = found_medium

        # dans .profile on prend la première ligne : label + référence
        product.description = (
            soup.find("div", class_="profile")
            .find("div", class_="content")
            .get_text(strip=True)
        )

        yield product

    def search_canary(self) -> bool:
        """Canary de recherche."""
        soup = self.fetch(self.search_url)
        if not soup:
            return False
        items = soup.find_all("tr", class_="shortcut_navigable")
        if not items:
            raise DigException("Liste des résultats de la recherche introuvable")
        for item in items:
            if not item.find("a", class_="item_description_title"):
                raise DigException(
                    "Lien vers page produit dans les résultat de la recherche"
                )
        return True

    def extract_canary(self) -> bool:
        """Canary de dig.

        On prend le premier lien .item_description_title sur la home
        de la marketplace et on teste l'extraction
        """
        soup = self.fetch(self.search_url)
        if not soup:
            return False
        link = soup.find("a", class_="item_description_title")
        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = self.base_url + "/" + link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la produit")
