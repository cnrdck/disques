"""eBay.

Beaucoup de variables inutiles & type: ignore pour contourner le fait qu'un item
(objet retourné en réponse d'une recherche) ne soit pas correctement décrit dans le sdk
et donc impossible à détailler dans les annotation de Digger.items
"""

from collections.abc import Iterator

from django.conf import settings
from djmoney.money import Money
from ebaysdk.exception import ConnectionError as EbayConnectionError
from ebaysdk.finding import Connection as Finding
from search.diggers import CrateDigger, logger
from search.models import Product


def get_description(item) -> str:
    """Description à partir des infos."""
    description = ["Vendu"]
    try:
        listing_type = item.listingInfo.listingType
    except AttributeError:
        listing_type = ""
    if listing_type in ["Auction", "AuctionWithBIN"]:
        description.append("aux enchères")
    else:
        description.append("à prix fixe")

    try:
        info = item.sellerInfo
        seller = f"par {info.sellerUserName} ({info.positiveFeedbackPercent}%)"
    except AttributeError:
        seller = ""
    if seller:
        description.append(seller)

    try:
        until = f"jusqu’au {item.listingInfo.endTime:%d-%m-%Y %H:%M}"
    except (AttributeError, ValueError):  # info manquante ou date invalide
        until = ""
    if until:
        description.append(until)

    try:
        info = item.shippingInfo
        shipping = f"frais de port estimés à {info.shippingServiceCost.value}€"
    except AttributeError:
        shipping = ""
    if shipping:
        description.append(shipping)

    return " ".join(description)


class Digger(CrateDigger):
    """eBay."""

    base_url = "https://www.ebay.fr"
    item_url = base_url + "/itm/"
    domain = "svcs.ebay.com"  # API endpoint
    uri = "/services/search/FindingService/v1"  # API endpoint uri
    search_url = "http://" + domain + uri
    site_id = "EBAY-FR"
    country: str = "FR"
    items: dict[str, object] = {}

    @property
    def availability_matchings(self):
        """Correspondance stocks.

        En stock = In Stock
        Backorder = Not in stock (les items complètement soldout sont masqués)
        Pré commande = Pre-order
        """
        return {
            "Active": settings.AVAILABILITY_IN,
            "Cancelled": settings.AVAILABILITY_SO,
            "Ended": settings.AVAILABILITY_SO,
            "EndedWithSales": settings.AVAILABILITY_SO,
            "EndedWithoutSales": settings.AVAILABILITY_BO,
        }

    def __init__(self, query, medium=None, shop=None):
        """Avec le `shop` fourni on modifie les propriétés du digger."""
        super().__init__(shop=shop, query=query, medium=medium)
        # if shop:
        #     self.country = shop.country
        self.api = Finding(
            appid=settings.EBAY_APP_ID,
            config_file=None,
            siteid=self.site_id,
            domain=self.domain,
        )

    @property
    def media_matchings(self) -> dict[str, str]:
        """On utilise le primaryCategory.categoryId pour traduire le format."""
        return {
            "176985": settings.MEDIA_VINYL,
            "176984": settings.MEDIA_CD,
            "176983": settings.MEDIA_K7,
        }

    def fetch(self, url=None, payload=None, verb="findItemsAdvanced") -> list | None:
        """Appelle l'API avec les paramètres transmis et ajoute un filtre country."""
        logger.info("Appel API %s", verb)
        logger.debug("Payload %s", payload)

        if payload is None:
            payload = {}

        if self.country:
            payload["itemFilter"] = {"name": "LocatedIn", "value": self.country}
        payload["outputSelector"] = "SellerInfo"

        try:
            response = self.api.execute(verb, payload)
        except EbayConnectionError as error:
            logger.error("Erreur HTTP %s", error)
            return None
        try:
            return response.reply.searchResult.get("item", [])
        except AttributeError:
            # 'ResponseDataObject' object has no attribute 'errorMessage'
            logger.error(
                "Erreur API id: %d, message: %s",
                response.reply.errorMessage.error.errorId,
                response.reply.errorMessage.error.message,
            )
            return None

    def search(self) -> Iterator[str]:
        """Recherche avancée avec filtre par catégorie.

        Les résultats sont mis en cache dans self.items après une recherche
        L'url retournée est celle de l'item sans le slug
        """
        categories = {
            v: k for k, v in self.media_matchings.items()
        }  # l'inverse de media_matchings, format: id
        payload = {
            "keywords": self.query,
            "categoryId": categories.get(self.medium, "11233"),
        }
        response = self.fetch(self.search_url, payload=payload)
        if not response:
            return
        for item in response:
            logger.debug("Trouvé le lien %s pour %s", item.viewItemURL, item.title)
            url = self.item_url + item.itemId
            self.items[url] = item  # mise en cache
            yield url

    def extract(self, url: str) -> Iterator["Product"]:
        """Extraction des données de l'Item."""
        item = self.items.get(url)
        if not item:  # pas en cache
            item_id = url.split("/")[-1]
            payload = {"keywords": item_id}
            response = self.fetch(url, payload=payload)
            if not response:
                return
            item = response[0]

        product = Product()
        product.shop = self.shop

        found_medium = self.media_matchings.get(
            item.primaryCategory.categoryId, settings.MEDIA_UNK  # type: ignore
        )
        # media ne matche pas avec la recherche (et un media est spécifié)
        # on ignore cet enregistrement et on passe au suivant
        if not self.media_match(found_medium):
            logger.info("Média ne match pas")
            return
        product.medium = found_medium

        product.title = item.title  # type: ignore

        product.url = url

        product.availability = self.availability_matchings.get(
            item.sellingStatus.sellingState, settings.AVAILABILITY_SO  # type: ignore
        )

        product.price = Money(
            float(item.sellingStatus.currentPrice.value),  # type: ignore
            item.sellingStatus.currentPrice._currencyId,  # type: ignore
        )

        product.description = get_description(item)

        product.image_url = None
        # plusieurs champs différents pour trouver une image
        # on tente par ordre de préférence
        for attr in [
            "pictureURLSuperSize",
            "pictureURLLarge",
            "galleryURL",
            "galleryPlusPictureURL",
        ]:
            try:
                product.image_url = getattr(item, attr)
                break
            except AttributeError:
                pass
        try:
            condition_id = item.condition.conditionId  # type: ignore
            product.condition = (
                settings.CONDITION_NEW
                if condition_id == "1000"
                else settings.CONDITION_USED
            )
        except AttributeError:
            product.condition = settings.CONDITION_UNK

        yield product

    def search_canary(self) -> bool:
        """L'api d'ebay est plus stable que les sites qu'on scrape...

        On teste quand même une recherche bateau.
        """
        payload = {
            "keywords": self.query,
            "categoryId": "11233",
            "paginationInput": {"entriesPerPage": "1"},
        }
        try:
            response = self.api.execute("findItemsAdvanced", payload)
        except EbayConnectionError as error:
            logger.error("Erreur HTTP %s", error)
            return False
        return response.reply.ack == "Success"

    def extract_canary(self) -> bool:
        """Search_canary suffit."""
        return True
