"""Bandcamp.

Le moteur de recherche étant en carton on passe par ddg pour récupérer des liens.
le parser lxml ne fonctionne pas sur certaines pages, on utilise html5lib à la place
ex.
* ko : https://tahiti80.bandcamp.com/album/the-sunsh-ne-beat-vol-1
* OK : https://rosiethomas.bandcamp.com/album/hit-run-vol-1
"""

from typing import TYPE_CHECKING
from urllib.parse import parse_qs, urlparse

from core.helpers import extract_amount, extract_currency
from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, logger
from search.models import Product

if TYPE_CHECKING:
    from bs4 import BeautifulSoup


def get_image_url(soup: "BeautifulSoup") -> str | None:
    """URL de la pochette du disque."""
    try:
        return soup.find("div", id="tralbumArt").a.img["src"]
    except AttributeError:
        return None


class Digger(CrateDigger):
    """Digger Bandcamp."""

    base_url = "https://bandcamp.com"
    search_url = "https://duckduckgo.com/html/"
    extra_query = "inurl:bandcamp.com/album"
    parser = "html5lib"  # lxml n'arrive pas à parser certaines pages bandcamp
    proxied = False
    search_limit = slice(0, 15)  # ex. :5 arrête de crawler après 5 résultats de ddg

    def __init__(self, query, medium=None, shop=None):
        """Si shop, on change la base_url & extra query l'ajouter à la requête."""
        super().__init__(query=query, medium=medium, shop=shop)
        if shop is not None:
            self.base_url = shop.url
            self.extra_query = self.extra_query.replace(
                "bandcamp.com", urlparse(shop.url).netloc
            )

    @property
    def media_matchings(self):
        """Correspondance media."""
        return {
            "vinyl": settings.MEDIA_VINYL,
            "compact disc": settings.MEDIA_CD,
            "cassette": settings.MEDIA_K7,
        }

    def media_guess(self, medium: str) -> str:
        """On cherche des mots clés dans une ligne "format" sur le descriptif du produit.

        ex.
        * cassette limited to 150 copies
        * 180 gram vinyl LP + MP3 download
        """
        for needle, match in self.media_matchings.items():
            if needle in medium.lower():
                return match
        return settings.MEDIA_UNK

    def media_match(self, medium: str) -> bool:
        """Correspondance media."""
        logger.debug("Media trouvé: '%s', cible: '%s'", medium, self.medium)
        if self.medium == settings.MEDIA_UNK:
            return True
        return self.medium == medium

    def search(self):
        """Recherche sur ddg.

        les résultats n'ont pas d'url directe, elles sont de la forme :
        /l/?kh=-1&uddg=<l'url encodée>
        """
        payload = {}
        payload["q"] = f"{self.extra_query} {self.query}"
        soup = self.fetch(self.search_url, payload)
        for item in soup.find_all("div", class_="result")[self.search_limit]:
            ddg_url = item.find("a", class_="result__url")["href"]
            parsed_url = urlparse(ddg_url)
            qs = parse_qs(parsed_url.query)
            if qs.get("uddg") is None:  # parfois des résultats sans url...
                continue
            url = qs["uddg"][0]
            logger.debug("Trouvé le lien %s", url)
            yield url

    def extract(self, url: str):
        """Extraction des infos."""
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return
        # j'aime pas leur façon de nommer les œuvres (titre, by artiste),
        # je change l'ordre
        page_title = soup.find("meta", property="og:title").get("content")
        pieces = page_title.split(", by", maxsplit=1)
        title = " - ".join(pieces[::-1])

        if self.query and not self.title_match(title):
            return
        product = Product()
        product.shop = self.shop
        product.condition = settings.CONDITION_NEW  # que du neuf
        product.image_url = get_image_url(soup)
        for box in soup.find_all("li", class_="buyItem"):
            if any(
                avoid in box["class"] for avoid in ["digital", "buyFullDiscography"]
            ):
                logger.debug("Numérique, on ignore")
                continue

            product.title = title
            try:  # plusieurs item par page, on ajoute l'id Bandcamp
                pid = box.find("h3").find("span")["id"].split("-")[-1]
            except KeyError:
                pid = None
            if not self.product_match_url(pid, url):
                logger.info("URL (pid) ne match pas")
                continue
            product.url = self.product_url(url, pid)
            product.description = box.find("div", class_="bd").get_text(" ", strip=True)
            try:
                btn = box.find("h4", class_="main-button").find(
                    "button"
                )  # si bouton c'est commandable, sinon sold-out
            except AttributeError:
                logger.debug("Sold-out, on ignore")
                continue
            action = btn.get_text(strip=True)
            found_medium = self.media_guess(action)
            if not self.media_match(found_medium):
                logger.debug("Mauvais format, on ignore")
                continue
            product.medium = found_medium
            # pas facile de différencier précommande/stock
            product.availability = settings.AVAILABILITY_IN
            amount_and_currency = (
                box.find("span", class_="nobreak").find("span").get_text()
            )
            currency = extract_currency(amount_and_currency)
            amount = extract_amount(amount_and_currency)
            product.price = Money(amount, currency)
            yield product
