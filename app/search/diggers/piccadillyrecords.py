"""Piccadilly records.

* recherche https://www.piccadillyrecords.com/counter/search.php?search=tame%20impala
"""

import re

from django.conf import settings
from djmoney.money import Money
from search.diggers import CrateDigger, DigException, logger
from search.models import Product

PRICE_REGEX = r"£(\d{1,3}\.\d{2})"


class Digger(CrateDigger):
    """Piccadilly records."""

    base_url = "https://www.piccadillyrecords.com"
    search_url = base_url + "/counter/search.php"
    currency = "GBP"

    @property
    def media_matchings(self):
        """Correspondances de format."""
        return {
            "icon_lp": settings.MEDIA_VINYL,
            "icon_7inch": settings.MEDIA_VINYL,
            "icon_cd": settings.MEDIA_CD,
            "icon_cassette": settings.MEDIA_K7,
            "icon_boxset": settings.MEDIA_VINYL,  # pas top
        }

    def media_guess(self, medium: str) -> str:
        """Pas de regex ici, juste un dictionnaire."""
        return self.media_matchings.get(medium, settings.MEDIA_UNK)

    def search(self):
        """Recherche sur leur moteur.

        Pas besoin de filtrer par format.
        """
        payload = {"search": self.query}
        soup = self.fetch(self.search_url, payload)
        # les résultats contiennent les liens vers les produits dans
        # a.stock-item-image-more-info
        for link in soup.find_all("a", class_="stock-item-image-more-info_new"):
            logger.debug("Trouvé le lien %s", link["href"])
            yield link["href"]

    def extract(self, url: str):
        """Extraction des données.

        Les métadonnées donnent quelques infos sur le produit.
        Ensuite tout se trouve dans des div.addtocart_box pour chaque option d'achat.
        """
        logger.info("Extraction des données depuis %s", url)
        soup = self.fetch(url)
        if not soup:
            return

        for box in soup.find_all("li", class_="stock-item-purchase-format"):
            product = Product()
            product.shop = self.shop
            header = soup.find("header", class_="page-titles")
            product.title = f"{header.h1.get_text()} - {header.h2.get_text()}"
            image = soup.find("div", id="productslideshow").img
            product.image_url = image["src"]
            product.condition = settings.CONDITION_NEW  # que du neuf

            purchase_options = box.find("div", class_="stock-item-purchase-options")
            # si commandable on a un bouton basket ou précommande
            # sinon c'est juste wishlist et pas de prix
            if purchase_options.find("button", class_="btn-preorder"):
                product.availability = settings.AVAILABILITY_PO
                button = purchase_options.find("button", class_="btn-preorder")
            elif purchase_options.find("button", class_="btn-basket"):
                product.availability = settings.AVAILABILITY_IN
                button = purchase_options.find("button", class_="btn-basket")
            else:
                product.availability = settings.AVAILABILITY_SO
                button = purchase_options.find("button", class_="btn-wishlist")

            # pid du produit est dans data-id + data-format du bouton
            # cela nous permet d'obtenir une fausse url unique pour le produit
            pid = "{id}_{fid}".format(id=button["data-id"], fid=button["data-format"])
            if not self.product_match_url(pid, url):
                logger.debug("URL (pid) ne match pas")
                continue
            product.url = self.product_url(url, pid)

            details_list = box.dl

            # <dl class="icon_lp">
            medium_icon = details_list["class"][0]
            logger.debug("* Format local : %s", medium_icon)
            found_medium = self.media_guess(medium_icon)
            if not self.media_match(found_medium):
                logger.debug("Mauvais format, on ignore")
                continue
            product.medium = found_medium

            # <div class="stock-item-price">
            # <dt>LP Box Set</dt><dd>£59.99 + <a href="#" (...)>shipping</a></dd>
            # </div>
            media_and_price = details_list.find("div", class_="stock-item-price")

            gbp_price = re.search(PRICE_REGEX, media_and_price.dd.get_text())

            if not gbp_price:
                logger.debug("Prix introuvable, on ignore")
                continue
            logger.debug("* devise + prix : %s", gbp_price.group(1))
            product.price = Money(float(gbp_price.group(1)), self.currency)
            description = []
            for child in details_list.find("div", class_="stock-item-info").children:
                if child.string and child.string.strip():
                    description.append(child.string.strip())
            product.description = " ".join(description)

            yield product

    def search_canary(self) -> bool:
        """Canary."""
        payload = {"search": "tame impala"}
        soup = self.fetch(self.search_url, payload)
        if not soup:
            return False
        items = soup.find_all("a", class_="stock-item-image-more-info_new")
        if not items:
            raise DigException("Liste des résultats de la recherche introuvable")
        return True

    def extract_canary(self) -> bool:
        """On prend le premier lien vers /records/... sur la home et on extrait.

        Raises:
            DigException: pas de fiche produit.
        """
        soup = self.fetch(self.base_url)
        if not soup:
            return False
        link = soup.select_one("a[href*=/prod/]")
        if not link:
            raise DigException("Lien vers page produit introuvable")
        url = link["href"]
        for _ in self.extract(url):
            return True  # on a une fiche produit
        raise DigException("Rien à extraire de la produit")
