"""Modèles Search."""

import os
import traceback
import uuid
from datetime import timedelta
from functools import partial
from typing import TYPE_CHECKING

from core.helpers import notify
from django.conf import settings
from django.core.validators import MinLengthValidator
from django.db import models, transaction
from django.db.models import QuerySet
from django.db.models.manager import BaseManager
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.module_loading import import_string
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django_q.tasks import async_task
from djmoney.contrib.exchange.models import convert_money
from djmoney.models.fields import MoneyField

if TYPE_CHECKING:
    from .diggers import CrateDigger


def unique_slugify(instance: models.Model, string: str) -> str:
    """Assurance qu'un slug soit unique.

    Args:
        instance: une instance de modèle.
        string: la chaîne à slugifier.

    Returns:
        Un slug unique pour `instance`.
    """
    Model = instance.__class__
    unique_slug = slug = slugify(string)[:50]  # limite SlugField à 50 chars par défaut
    while Model.objects.filter(slug=unique_slug).exists():
        if len(slug) > 45:
            slug = slug[:45]
        unique_slug = f"{slug}-{get_random_string(length=4)}"
    return unique_slug


def digger_choices() -> list[tuple[str, str]]:
    """Liste les diggers disponibles."""
    diggers = []
    with os.scandir(settings.DIGGERS_PATH) as entries:
        for entry in entries:
            if not entry.name.startswith("__") and entry.name.endswith(".py"):
                digger, _ = os.path.splitext(entry.name)
                diggers.append((digger, digger))
    return diggers


def dig_it(dig_id: str) -> None:
    """Lancement d'une recherche sur un disquaire (aka un dig).

    Args:
        dig_id: id du dig à démarrer
    """
    dig = Dig.objects.get(pk=dig_id)
    dig.start()
    try:
        for product in dig.digger.dig():
            dig.keep(product)
    except Exception as exception:
        dig.failed(exception)
    else:
        dig.stop()


class Shop(models.Model):
    """Un disquaire."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_("nom"), max_length=150)
    slug = models.SlugField(_("slug"))
    url = models.URLField(_("URL"))
    digger_name = models.CharField(_("digger"), choices=digger_choices(), max_length=50)
    logo = models.URLField(_("Logo"), blank=True)
    icon = models.URLField(_("Icône"), blank=True)
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)
    is_active = models.BooleanField(
        _("actif"),
        help_text=_("Un disquaire désactivé n’est plus testé/utilisé"),
        default=True,
    )
    description = models.TextField(
        _("informations complémentaires"),
        blank=True,
    )
    region = models.CharField(
        _("région"),
        choices=settings.REGION_CHOICES,
        max_length=2,
        blank=True,
    )
    is_independent = models.BooleanField(_("indépendant"), default=True)
    is_bm = models.BooleanField(_("physique"), default=False)
    is_marketplace = models.BooleanField(_("place de marché"), default=False)
    is_direct = models.BooleanField(
        _("direct"), help_text=_("Appartient au label / artiste"), default=False
    )

    class Meta:
        verbose_name = _("disquaire")
        verbose_name_plural = _("disquaires")
        ordering = ["name"]

    @property
    def digger_class(self) -> "CrateDigger":
        """Crate Digger utilisé pour le disquaire."""
        return import_string(f"search.diggers.{self.digger_name}.Digger")

    def __str__(self) -> str:
        """Retourne le nom du magasin."""
        return self.name

    def save(self, *args, **kwargs) -> None:
        """Génération du slug."""
        if not self.slug:
            self.slug = slugify(str(self))
        super().save(*args, **kwargs)

    def health_check(self) -> None:
        """Vérification de l'état de santé du magasin.

        Si trop de digs annulés dans un temps imparti on désactive le disquaire.
        """
        if not self.is_active:
            return

        if (
            Dig.objects.filter(
                shop=self,
                ended_at__gt=timezone.now() - settings.DIGS_MAX_FAILURE_WINDOW,
                status=settings.STATUS_CANCELLED,
            ).count()
            > settings.DIGS_MAX_FAILURE
        ):
            self.is_active = False
            self.save()
            message = "Dépassement du seuil d'erreur pour {self} 🙀"
            transaction.on_commit(partial(notify, message=message))


class Product(models.Model):
    """Un produit dans un magasin."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(_("slug"))
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name=_("products"))
    seller = models.CharField(_("vendeur"), max_length=100, blank=True)
    title = models.TextField(_("titre"), blank=False, null=False)
    image_url = models.URLField(
        _("URL de l’image"),
        blank=True,
    )
    medium = models.CharField(
        _("format"),
        choices=settings.MEDIA_CHOICES,
        max_length=10,
        default=settings.MEDIA_UNK,
    )
    url = models.URLField(_("URL"), blank=True, unique=True)
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)
    description = models.TextField(
        _("informations complémentaires"),
        blank=True,
    )
    price = MoneyField(
        _("prix"), max_digits=5, decimal_places=2, default_currency="EUR"
    )
    price_in_eur = MoneyField(
        _("prix en euros"),
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
        default_currency="EUR",
    )
    availability = models.CharField(
        _("disponibilité"),
        max_length=10,
        choices=settings.AVAILABILITY_CHOICES,
        default=settings.AVAILABILITY_UNK,
    )
    condition = models.CharField(
        _("état"),
        max_length=10,
        choices=settings.CONDITION_CHOICES,
        default=settings.CONDITION_UNK,
    )

    class Meta:
        verbose_name = _("produit")
        verbose_name_plural = _("produits")

    def __str__(self) -> str:
        """Retourne le titre, le format, le prix et le disquaire."""
        return f"{self.title} ({self.medium}) - {self.price} @ {self.shop}"

    def save(self, *args, **kwargs) -> None:
        """Mise à jour du prix en euros et slug."""
        if self.price:
            self.price_in_eur = convert_money(self.price, "EUR")
        if not self.slug:
            self.slug = unique_slugify(self, f"{self.shop.name} {self.title}")
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Page produit."""
        return reverse("product", args=[self.slug])

    @property
    def related_products(self) -> "QuerySet":
        """Produits liés (présents dans d'autres résultats de recherche)."""
        results_qs = Result.objects.filter(product=self).values("search")
        return (
            Product.objects.filter(sources__search__in=results_qs)
            .prefetch_related("shop")
            .distinct()
        )


class Search(models.Model):
    """Les recherches lancées sur le site."""

    QUERY_MIN_LENGTH = 3
    SEARCH_MAX_TTL = 24

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    query = models.TextField(
        _("recherche"), validators=[MinLengthValidator(QUERY_MIN_LENGTH)]
    )
    medium = models.CharField(
        _("format"),
        choices=settings.MEDIA_CHOICES,
        default=settings.MEDIA_UNK,
        max_length=10,
        blank=False,
    )
    slug = models.SlugField(_("slug"))
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    viewed_at = models.DateTimeField(_("dernière consultation"), blank=True, null=True)
    digged_at = models.DateTimeField(_("dernière exécution"), blank=True, null=True)

    class Meta:
        verbose_name = _("recherche")
        verbose_name_plural = _("recherches")
        constraints = [
            models.UniqueConstraint(
                fields=["query", "medium"], name="unique_query_medium"
            ),
        ]

    @property
    def title(self) -> str:
        """Raccourci vers __str__ en Title Case."""
        return str(self).title()

    @property
    def is_expired(self) -> bool:
        """True si la recherche a été lancé depuis plus de 24h."""
        if not self.digged_at or self.digged_at < timezone.now() - timedelta(
            hours=self.SEARCH_MAX_TTL
        ):
            return True
        return False

    @property
    def products(self) -> BaseManager["Product"]:
        """Raccourci vers les produits."""
        return Product.objects.filter(sources__search=self).all()

    @property
    def products_count(self) -> int:
        """Décompte des produits."""
        return Product.objects.filter(sources__search=self).count()

    @property
    def digging(self) -> bool:
        """A des digs en cours."""
        return self.digs.filter(
            status__in=[settings.STATUS_WAIT, settings.STATUS_RUNNING]
        ).exists()

    def __str__(self) -> str:
        """Retourne la requête + média (si précisé)."""
        if self.medium != settings.MEDIA_UNK:
            return f"{self.query} ({self.medium})"
        return self.query

    def save(self, *args, **kwargs):
        """Génération du slug et création des digs."""
        if not self.slug:
            self.slug = unique_slugify(self, str(self))
        super().save(*args, **kwargs)
        if self.is_expired:
            transaction.on_commit(self.start)

    def get_absolute_url(self):
        """Page correspondante à la recherche."""
        return reverse("shelf", args=[self.slug])

    def start(self) -> None:
        """Crée un dig pour chaque disquaire sur cette recherche."""
        for shop in Shop.objects.filter(is_active=True).all():
            Dig.objects.create(search=self, shop=shop)
        self.digged_at = timezone.now()
        self.save()


class Dig(models.Model):
    """Une recherche chez un disquaire."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    search = models.ForeignKey(Search, related_name="digs", on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, related_name="digs", on_delete=models.CASCADE)
    status = models.CharField(
        _("état"),
        max_length=10,
        choices=settings.STATUS_CHOICES,
        default=settings.STATUS_WAIT,
    )
    reason = models.TextField(
        _("raison de l'abandon"),
        blank=True,
    )

    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True, null=True)
    started_at = models.DateTimeField(_("début"), blank=True, null=True)
    ended_at = models.DateTimeField(_("fin"), blank=True, null=True)

    class Meta:
        verbose_name = _("dig")
        verbose_name_plural = _("digs")

    @property
    def duration(self) -> int | None:
        """Temps, en secondes, passé à digger."""
        if not self.started_at or not self.ended_at:
            return None
        return round((self.ended_at - self.started_at).total_seconds())

    @property
    def digger(self) -> "CrateDigger":
        """Le crate digger instancié."""
        return self.shop.digger_class(
            query=self.search.query, medium=self.search.medium, shop=self.shop
        )

    def start(self) -> None:
        """Passe le dig en running et met à jour la date."""
        self.status = settings.STATUS_RUNNING
        self.started_at = timezone.now()
        self.save()

    def stop(self) -> None:
        """Passe le dig en done et met à jour la date."""
        self.status = settings.STATUS_DONE
        self.ended_at = timezone.now()
        self.save()

    def failed(self, exception: Exception | None = None) -> None:
        """Passe le dig en cancelled."""
        self.status = settings.STATUS_CANCELLED
        self.ended_at = timezone.now()
        if exception:
            self.reason = "".join(
                traceback.TracebackException.from_exception(exception).format()
            )
        self.save()

    def keep(self, product_info: Product) -> "Result":
        """Transforme un produit trouvé par un digger en un résultat de recherche.

        On commence par créer / mettre à jour le produit puis on fait pareil pour le
        résultat de la recherche.
        """
        product, __ = Product.objects.update_or_create(
            url=product_info.url,
            shop=self.shop,
            defaults=model_to_dict(
                product_info, exclude=["created_at", "updated_at", "url", "shop"]
            ),
        )
        result, __ = Result.objects.get_or_create(
            search=self.search, dig=self, product=product
        )
        return result

    def __str__(self) -> str:
        """Retourne le détail et l’état du dig."""
        return f"{self.search} @{self.shop} ({self.status})"

    def save(self, *args, **kwargs) -> None:
        """Lancement du worker."""
        super().save(*args, **kwargs)
        if not self.started_at:
            transaction.on_commit(
                lambda: async_task("search.models.dig_it", str(self.pk))
            )


class Result(models.Model):
    """Un résultat de dig (lien entre un produit la recherche et le dig)."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    search = models.ForeignKey(Search, related_name="results", on_delete=models.CASCADE)
    dig = models.ForeignKey(Dig, related_name="results", on_delete=models.CASCADE)
    product = models.ForeignKey(
        Product, related_name="sources", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(_("création"), auto_now_add=True)

    class Meta:
        verbose_name = _("résultat")
        verbose_name_plural = _("résultats")
        constraints = [
            models.UniqueConstraint(
                fields=["search", "dig", "product"], name="unique_result"
            ),
        ]

    def __str__(self) -> str:
        return f"{self.product} ({self.dig})"
