"""Formulaires."""

from typing import TYPE_CHECKING

from django import forms
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext as _

from .models import Search

if TYPE_CHECKING:
    from django.db.models import QuerySet


class SearchForm(forms.ModelForm):
    """Formulaire pour lancer une recherche."""

    class Meta:
        model = Search
        fields = ["query"]
        widgets = {"query": forms.TextInput}

    def validate_unique(self):
        """On ignore les validateurs d'unicité.

        On a une contrainte d'unicité sur `query` + `medium`, elle est prise en compte
        sur la méthode save en utilisant un `update_or_create`.
        """
        pass

    def save(self, commit=True) -> Search:
        """Les recherches sont uniques par `query` & medium, on `update_or_create`."""
        search, __ = Search.objects.update_or_create(
            query=self.cleaned_data["query"],
            defaults={"viewed_at": timezone.now()},
        )
        return search


class SearchFilterForm(forms.Form):
    """Formulaire de filtrage d'un bac."""

    MEDIUM_CHOICES = [("", _("Format"))] + settings.MEDIA_CHOICES
    AVAILABILITY_CHOICES = [("", _("Stock"))] + settings.AVAILABILITY_CHOICES
    CONDITION_CHOICES = [("", _("État"))] + settings.CONDITION_CHOICES

    query = forms.CharField(label=_("Filtre"), max_length=100, required=False)
    medium = forms.ChoiceField(
        label=_("Format"), choices=MEDIUM_CHOICES, required=False
    )
    availability = forms.ChoiceField(
        label=_("Stock"), choices=AVAILABILITY_CHOICES, required=False
    )
    condition = forms.ChoiceField(
        label=_("État"), choices=CONDITION_CHOICES, required=False
    )

    def filter_products(self, queryset: "QuerySet") -> "QuerySet":
        """Filtre la requête de récupération des produits.

        A utiliser après un `form.is_valid()` car on utilise `cleaned_data`

        Args:
            queryset: la requête produit

        Returns:
            La requête filtrée.
        """
        filters = self.cleaned_data
        if filters["query"]:
            queryset = queryset.filter(title__icontains=filters["query"])
        if filters["medium"]:
            queryset = queryset.filter(medium=filters["medium"])
        if filters["availability"]:
            queryset = queryset.filter(availability=filters["availability"])
        if filters["condition"]:
            queryset = queryset.filter(condition=filters["condition"])
        return queryset
