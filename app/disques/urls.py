"""Routage."""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView
from search.views import home, product, search, shelf

urlpatterns = (
    [
        path("", home, name="home"),
        path(
            "recherche",
            search,
            name="search",
        ),
        path(
            "bac/<str:slug>",
            shelf,
            name="shelf",
        ),
        path(
            "produit/<str:slug>",
            product,
            name="product",
        ),
        path("admin/", admin.site.urls),
        path(
            "sw.js",
            TemplateView.as_view(
                template_name="sw.js", content_type="application/javascript"
            ),
            name="sw.js",
        ),
        path(
            "manifest.json",
            TemplateView.as_view(
                template_name="manifest.json", content_type="application/json"
            ),
            name="manifest.json",
        ),
        path(
            "offline.html",
            TemplateView.as_view(
                template_name="offline.html",
            ),
            name="offline.html",
        ),
        path("__debug__/", include("debug_toolbar.urls")),
    ]
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)
