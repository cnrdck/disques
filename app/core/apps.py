"""Core apps."""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CoreConfig(AppConfig):
    """Core config."""

    name = "core"
    verbose_name = _("core")
