"""Customisation de sorl-thumbnail.

Juste pour avoir notre user agent quand sorl récupère les images depuis les sites,
discogs bloque celui utilisé par défaut (python-urllib/0.6).
"""

import logging
from urllib.error import HTTPError, URLError
from urllib.request import Request, urlopen

from django.conf import settings
from django.utils.encoding import force_str
from sorl.thumbnail import default
from sorl.thumbnail.base import ThumbnailBackend as OriginalThumbnailBackend
from sorl.thumbnail.conf import defaults as default_settings
from sorl.thumbnail.conf import settings as sorl_settings
from sorl.thumbnail.default import storage as default_storage
from sorl.thumbnail.helpers import ThumbnailError
from sorl.thumbnail.images import DummyImageFile, url_pat
from sorl.thumbnail.images import ImageFile as OriginalImageFile
from sorl.thumbnail.images import UrlStorage as OriginalUrlStorage

logger = logging.getLogger(__name__)


class UrlStorage(OriginalUrlStorage):
    """Surcharge de open de l'UrlStorage original pour spécifier l'user agent."""

    def open(self, name, mode="rb"):
        """Open."""
        url = self.normalize_url(name)
        req = Request(url, headers={"User-Agent": settings.USER_AGENT})  # noqa: S310
        return urlopen(req, timeout=sorl_settings.THUMBNAIL_URL_TIMEOUT)  # noqa: S310

    def exists(self, name):
        """Exists."""
        try:
            self.open(name)
        except (HTTPError, URLError):
            return False
        return True


class ImageFile(OriginalImageFile):
    """Surcharge de l'init de l'ImageFile original pour charger notre UrlStorage."""

    def __init__(self, file_, storage=None):  # noqa: C901
        """Init."""
        if not file_:
            raise ThumbnailError("File is empty.")

        # figure out name
        if hasattr(file_, "name"):
            self.name = file_.name
        else:
            self.name = force_str(file_)

        # TODO: Add a customizable naming method as a signal

        # Remove query args from names. Fixes cache and signature arguments
        # from third party services, like Amazon S3 and signature args.
        if sorl_settings.THUMBNAIL_REMOVE_URL_ARGS:
            self.name = self.name.split("?")[0]

        # Support for relative protocol urls
        if self.name.startswith("//"):
            self.name = "http:" + self.name

        # figure out storage
        if storage is not None:
            self.storage = storage
        elif hasattr(file_, "storage"):
            self.storage = file_.storage
        elif url_pat.match(self.name):
            # Changement de l'UrlStorage original
            self.storage = UrlStorage()
        else:
            self.storage = default_storage

        if hasattr(self.storage, "location"):
            location = self.storage.location
            if not self.storage.location.endswith("/"):
                location += "/"
            if self.name.startswith(location):
                self.name = self.name[len(location) :]


class ThumbnailBackend(OriginalThumbnailBackend):
    """Surcharge de get_thumbnail de ThumbnailBackend pour charger notre ImageFile."""

    def get_thumbnail(self, file_, geometry_string, **options):
        """Get thumbnail."""
        logger.debug("Getting thumbnail for file [%s] at [%s]", file_, geometry_string)

        if file_:
            source = ImageFile(file_)
        else:
            raise ValueError("falsy file_ argument in get_thumbnail()")

        # preserve image filetype
        if sorl_settings.THUMBNAIL_PRESERVE_FORMAT:
            options.setdefault("format", self._get_format(source))

        for key, value in self.default_options.items():
            options.setdefault(key, value)

        # For the future I think it is better to add options only if they
        # differ from the default settings as below. This will ensure the same
        # filenames being generated for new options at default.
        for key, attr in self.extra_options:
            value = getattr(sorl_settings, attr)
            if value != getattr(default_settings, attr):
                options.setdefault(key, value)

        name = self._get_thumbnail_filename(source, geometry_string, options)
        thumbnail = ImageFile(name, default.storage)
        cached = default.kvstore.get(thumbnail)

        if cached:
            return cached

        # We have to check exists() because the Storage backend does not
        # overwrite in some implementations.
        if sorl_settings.THUMBNAIL_FORCE_OVERWRITE or not thumbnail.exists():
            try:
                source_image = default.engine.get_image(source)
            except Exception as e:
                logger.exception(e)
                if sorl_settings.THUMBNAIL_DUMMY:
                    return DummyImageFile(geometry_string)
                else:
                    # if storage backend says file doesn't exist remotely,
                    # don't try to create it and exit early.
                    # Will return working empty image type; 404'd image
                    logger.warning(
                        "Remote file [%s] at [%s] does not exist",
                        file_,
                        geometry_string,
                    )
                    return thumbnail

            # We might as well set the size since we have the image in memory
            image_info = default.engine.get_image_info(source_image)
            options["image_info"] = image_info
            size = default.engine.get_image_size(source_image)
            source.set_size(size)

            try:
                self._create_thumbnail(
                    source_image, geometry_string, options, thumbnail
                )
                self._create_alternative_resolutions(
                    source_image, geometry_string, options, thumbnail.name
                )
            finally:
                default.engine.cleanup(source_image)

        # If the thumbnail exists we don't create it, the other option is
        # to delete and write but this could lead to race conditions so I
        # will just leave that out for now.
        default.kvstore.get_or_set(source)
        default.kvstore.set(thumbnail, source)
        return thumbnail
